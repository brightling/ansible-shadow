SHADOW
======

Manage the shadow package configuration used for managing users and groups accounts on systems.

Requirements
------------

Ansible 2.0+

Role Variables
--------------

### Main Role Variables

* `shadow_state1`: Action for the role to take.
  * _present_(Default): Shadow packages are present and configured as specified.
  * _absent_: Shadow packages and configurations are absent.

### Login.defs Variables

* `shadow_login_defs`: Dictonary of key:values to represent login.defs items.

### default/useradd Variables

* `shadow_default_useradd`: Dictonary of key:values to represent default/useradd items.

Dependencies
------------

None

Example Playbook
----------------

Install and use default shadow configuration.

    - hosts: servers
      roles:
         - { role: brightlingmeuk.shadow }

Install and set the a minimal configuration.

    - hosts: servers
      roles:
        - { role: brightlingmeuk.shadow,
            shadow_login_defs: { env_path: '/bin:/usr/bin',
                                 env_supath: 'PATH=/sbin:/bin:/usr/sbin:/usr/bin',
                                 mail_dir: '/var/mail'
                               }
          }

Install and append to system configuration.

    - hosts: servers
      roles:
        - { role: brightlingmeuk.shadow,
            shadow_login_defs: "{{ _shadow_login_defs|combine(
            {'log_ok_logins': true,
             'login_retries': 3})
            }}
          }

License
-------

GPLv3

Author Information
------------------

- Robert White | [e-mail](mailto:sentryveil00@gmail.com) | [GitLab](https://gitlab.com/brightling)
